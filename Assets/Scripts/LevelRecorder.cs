﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class LevelRecorder : MonoBehaviour {

	LevelObject level;
	List<LevelObject.MoveObject> moves;

	public static LevelRecorder current;

	public void Init(string map) {
		current = this;
		level = new LevelObject ();
		moves = new List<LevelObject.MoveObject> ();
		level.map = map;
	}

	public static void AddMove(int lvl, Vector2 pos, int xDirection, int yDirection, bool flip) {
		LevelObject.MoveObject move = new LevelObject.MoveObject ();
		move.lvl = lvl;
		move.pos = pos;
		move.xd = xDirection;
		move.yd = yDirection;
		move.flip = flip;

		current.moves.Add (move);
	}

	public static void Finish(float time) {
		current.level.time = time;
		current.level.minSpawns = SlimeSpawn.spawnCouter;
		current.level.moves = current.moves.ToArray ();
	}

	public void Send() {
		Debug.Log (JsonUtility.ToJson(level));
		StartCoroutine (Upload());
	}

	IEnumerator Upload() {
		WWWForm form = new WWWForm();
		form.AddField("data", JsonUtility.ToJson(level));
		UnityWebRequest www = UnityWebRequest.Post("http://fancyreckless.org/cgi-bin/putBeta/", form);
		yield return www.Send();

		if(www.isNetworkError) {
			Debug.Log(www.error);
		}
		else {
			Debug.Log("Form upload complete!");
		}
	}
}
