﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flipper : MonoBehaviour {

	[HideInInspector]public bool faded = false;

	public void Fade() {
		faded = true;
		StartCoroutine(UnFade ());
	}

	IEnumerator UnFade() {
		yield return new WaitForSeconds(0.2f);
		faded = false;
	}
}
