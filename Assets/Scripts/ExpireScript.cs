﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExpireScript : MonoBehaviour {
	public float timeout = 0.3f;

	void Start () {
		Invoke ("Expire", timeout);
	}

	void Expire () {
		Destroy (gameObject);
	}
}
