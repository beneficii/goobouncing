﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreObject {
	string name;
	public int defaultScore;
	bool moreIsBetter;

	public ScoreObject(string scoreName, int scoreDefault, bool betterMore) {
		name = scoreName;
		defaultScore = scoreDefault;
		moreIsBetter = betterMore;
	}

	public void setScore(int newScore, int level = 0) {
		int prevScore = PlayerPrefs.GetInt (fullName(level), defaultScore);
		if ((moreIsBetter && newScore > prevScore) ||
		   (!moreIsBetter && newScore < prevScore)) {
			PlayerPrefs.SetInt (fullName(level), newScore);
		}
	}

	public int getScore(int level = 0) {
		return PlayerPrefs.GetInt (fullName(level), defaultScore);
	}

	string fullName(int level) {
		return string.Format ("{0}[{1}]", name, level);
	}
}

public class ScoreScript : MonoBehaviour {
	public static ScoreObject scrLeastSpawns = new ScoreObject("PPREFS_LEAST_SPAWNS", 999, false);
	public static ScoreObject scrLevelsBeaten = new ScoreObject("PPREFS_LEVELS_BEATEN", 0, true);

	public static void levelFinished() {
		int level = LevelScript.levelIndex;
		scrLeastSpawns.setScore (SlimeSpawn.spawnCouter, level);
		scrLevelsBeaten.setScore (level+1);
	}

	public static int getStars(int level, int score = -1) {
		if (score == -1) {
			score = scrLeastSpawns.getScore (level);
		}
		int minSpawns = LevelScript.getLevel (level).minSpawns;
		if (score == scrLeastSpawns.defaultScore) {
			return 0;
		} else if (score == minSpawns) {
			return 3;
		} else if (score - 1 == minSpawns) {
			return 2;
		} else if (score - 1 > minSpawns) {
			return 1;
		}

		return 3; //best score miscalculated
	}

	public static void setStars(Image[] images, int level, int score = -1) {
		int stars = getStars (level, score);
		for (int i = 0; i < images.Length; i++) {
			images [i].enabled = (i < stars);
		}
	}
}
