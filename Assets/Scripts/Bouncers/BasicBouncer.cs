﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicBouncer : MonoBehaviour, IBouncer {

	public void Bounce(SlimeScript slime, int x, int y) {
		slime.setSpeedModifier(1f);
		if (x == 0) { //horizontal platform
			slime.rb2d.velocity = Vector2.up *(y*5f);
		} else { //wall platform
			slime.flipSide (x);
		}
	}
}
