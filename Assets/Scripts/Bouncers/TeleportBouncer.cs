﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportBouncer : MonoBehaviour, IBouncer {

	public Vector2 endpoint;

	public void Bounce(SlimeScript slime, int x, int y) { //x,y - bouncers "up" coordinates
		slime.transform.position = endpoint;
		if (x == 0) { //horizontal platform
			slime.rb2d.velocity = -Vector2.up *(y*5f);
		} else { //wall platform
			//keep same direction
		}
	}
}