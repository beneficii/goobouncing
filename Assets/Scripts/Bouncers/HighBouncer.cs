﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HighBouncer : MonoBehaviour, IBouncer {

	public void Bounce(SlimeScript slime, int x, int y) {
		slime.setSpeedModifier(0.6f);
		if (x == 0) { //horizontal platform
			slime.rb2d.velocity = Vector2.up *(y*8f);
		} else { //wall platform
			slime.flipSide (x);
			slime.rb2d.velocity = Vector2.up * 5;
		}
	}
}
