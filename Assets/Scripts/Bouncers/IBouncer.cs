﻿public interface IBouncer {
	void Bounce(SlimeScript slime, int x, int y);
}