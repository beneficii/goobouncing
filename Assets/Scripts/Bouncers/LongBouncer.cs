﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LongBouncer : MonoBehaviour, IBouncer {

	public void Bounce(SlimeScript slime, int x, int y) {
		slime.setSpeedModifier(2.3f);
		if (x == 0) { //horizontal platform
			slime.rb2d.velocity = Vector2.up *(y*3.5f);
		} else { //wall platform
			slime.flipSide (x);
			slime.rb2d.velocity = Vector2.up *2.2f;
		}
	}
}
