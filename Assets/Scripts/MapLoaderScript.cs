﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class MapLoaderScript : MonoBehaviour {
	[Header("Prefabs")]
	public GameObject spawnPrefab;
	public GameObject exitPrefab;
	public GameObject keyPrefab;
	public GameObject spikePrefab;
	public GameObject powerPrefab;
	public GameObject flipPrefab;

	[Header("Sprites")]
	public Sprite[] wallSprite;
	//private
	delegate Vector2 IndexMapper(int i);

	char[,] map;
	int [,] boundGrid;
	int width, height;

	class MapBouncer {
		public int level;
		public int x;
		public int y;

		public MapBouncer (int lvl, int bx, int by) {
			level = lvl;
			x = bx;
			y = by;
		}
	}
	List<MapBouncer> bouncers;

	void Start() {
		init (LevelScript.getLevel().map);
	}

	public void init(string mapText) {
		bouncers = new List<MapBouncer> ();
		GetComponent<LevelRecorder> ().Init (mapText);
		generateArrays (mapText);
		adjustCamera ();

		//make bound colliders
		GameObject colliders = new GameObject("ColliderObject");
		colliders.tag = "Platform";
		generateBounds (colliders, i => new Vector2(i%width, i/width));
		generateBounds (colliders, i => new Vector2(i/height, i%height));

		//put sprites and prefabs
		generateObjects ();
		putBouncers ();
	}

	void generateArrays(string mapText) {
		width = mapText.Split ('|') [0].Length + 1;
		height = mapText.Split ('|').Length + 1;
		int x = 1, y = height - 1;
		boundGrid = new int[width, height];
		map = new char[width, height];

		for (int i = 0; i < mapText.Length; i++) {
			char c = mapText [i];
			if (c == '|') {
				y--;
				x = 1;
			} else {
				if (c == 'x') {
					boundGrid [x - 1, y - 1]++;
					boundGrid [x, y - 1]++;
					boundGrid [x - 1, y]++;
					boundGrid [x, y]++;
				}
				map [x, y] = c;
				x++;
			}
		}
	}

	void generateBounds(GameObject colliders, IndexMapper getxy) {
		List<Vector2> newVerticies = new List<Vector2> ();
		bool drawing = false;

		for (int i = 0; i < width * height; i++) {
			int x = (int)getxy(i).x;
			int y = (int)getxy(i).y;

			if (boundGrid [x, y] % 2 == 1) {
				if (!drawing) {
					drawing = true;
					newVerticies = new List<Vector2> ();
					newVerticies.Add (new Vector2 (x + 0.5f, y + 0.5f));
				} else {
					newVerticies.Add (new Vector2 (x + 0.5f, y + 0.5f));
					colliders.AddComponent<EdgeCollider2D> ().points = newVerticies.ToArray ();
					drawing = false;
				}
			}
		}
	}

	void putBouncers() {
		foreach (MapBouncer b in bouncers) {
			BouncerSpawn.AddMapBouncer(b.level, new Vector2(b.x, b.y));
		}
	}

	void adjustCamera() {
		float maxX = (float)width;
		Camera.main.orthographicSize = Mathf.Ceil(maxX * Screen.height / Screen.width * 0.5f);
		if (Camera.main.orthographicSize < height * 0.5f + 1f) {
			Camera.main.orthographicSize = Mathf.Ceil(height * 0.5f + 1f);
		}
		Camera.main.transform.position = new Vector3 (maxX/2, Camera.main.orthographicSize, -10);
	}

	/*
	void adjustCamera() { //for pixelPerfect
		float maxX = (float)width;
		PixelPerfectCamera cam = Camera.main.GetComponent<PixelPerfectCamera> ();
		print (cam.targetCameraHalfWidth);
		cam.targetDimension = PixelPerfectCamera.Dimension.Height;
		cam.targetCameraHalfHeight = maxX * Screen.height / Screen.width*0.5f;//maxX/2;
		cam.pixelPerfect = true;
		cam.adjustCameraFOV ();
		print (cam.targetCameraHalfWidth);
		Camera.main.transform.position = new Vector3 (maxX/2, Camera.main.orthographicSize+1, -10);
	}*/

	void generateObjects () {
		for (int x = 1;x<width;x++) {
			for (int y = 1; y < height; y++) {
				char c = map [x, y];
				switch (c) {
				case 'o':
					break;
				case 'x':
					putWall (x, y);
					break;
				default:
					putOther (x, y, c);
					break;
				}
			}
		}
	}

	int getWallSide(int x, int y) {
		try {
			if(map[x, y] == 'x') {
				return 1;
			} else {
				return 0;
			}
		} catch (IndexOutOfRangeException) {
			return 0;
		}

	}

	void putWall(int x, int y) {
		GameObject instance = new GameObject ();
		instance.transform.position = new Vector3 (x, y, 0);
		SpriteRenderer sRenderer = instance.AddComponent<SpriteRenderer> ();

		//get needed sprite
		int index = 0;

		index += getWallSide (x, y+1) * 1;
		index += getWallSide (x+1, y) * 2;
		index += getWallSide (x, y-1) * 4;
		index += getWallSide (x-1, y) * 8;

		sRenderer.sprite = wallSprite[index];
	}

	void putOther(int x, int y, char type) {
		GameObject instance = null;
		switch (type) {
		case 'd':
			instance = Instantiate (spikePrefab);
			break;
		case 'e':
			instance = Instantiate (exitPrefab);
			break;
		case 'k':
			instance = Instantiate (keyPrefab);
			break;
		case 's':
			instance = Instantiate (spawnPrefab);
			instance.GetComponent<SlimeSpawn>().level = 0;
			break;
		case '!':
			instance = Instantiate (spawnPrefab);
			instance.GetComponent<SlimeSpawn>().level = 1;
			break;
		case '@':
			instance = Instantiate (spawnPrefab);
			instance.GetComponent<SlimeSpawn>().level = 2;
			break;
		case '#':
			instance = Instantiate (spawnPrefab);
			instance.GetComponent<SlimeSpawn>().level = 3;
			break;
		case 'f':
			instance = Instantiate (flipPrefab);
			break;
		case 'p':
			instance = Instantiate (powerPrefab);
			instance.GetComponent<PowerUpScript> ().init (2);
			break;
		case '0':
			instance = Instantiate (powerPrefab);
			instance.GetComponent<PowerUpScript> ().init (0);
			break;
		case '1':
			instance = Instantiate (powerPrefab);
			instance.GetComponent<PowerUpScript> ().init (1);
			break;
		case '2':
			instance = Instantiate (powerPrefab);
			instance.GetComponent<PowerUpScript> ().init (2);
			break;
		case '3':
			instance = Instantiate (powerPrefab);
			instance.GetComponent<PowerUpScript> ().init (3);
			break;
		case '$':
			bouncers.Add (new MapBouncer (0, x, y));
			return;
		case '%':
			bouncers.Add (new MapBouncer (1, x, y));
			return;
		case '^':
			bouncers.Add (new MapBouncer (2, x, y));
			return;
		case '&':
			bouncers.Add (new MapBouncer (3, x, y));
			return;
		default:
			return;
		}
		instance.transform.position = new Vector3 (x, y, 0);
	}
}
