﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour {

    public List<GameObject> CheatButtons;

    bool _cheatEnabled = false;

	public void OnButtonPlay()
    {
        SceneManager.LoadScene("Levels");
    }

    public void ResetAll()
    {
        PlayerPrefs.DeleteAll();
    }

    public void UnlockNextLevel()
    {
        ScoreScript.scrLevelsBeaten.setScore(ScoreScript.scrLevelsBeaten.getScore() + 1);
    }

    public void ToggleCheat()
    {
        _cheatEnabled = !_cheatEnabled;

        foreach(var button in CheatButtons)
        {
            button.SetActive(_cheatEnabled);

        }
    }
}
