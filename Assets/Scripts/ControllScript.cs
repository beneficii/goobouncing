﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ControllScript : MonoBehaviour {
	public GameObject winPanel;
	public Text winPanelText;
	public GameObject menuPanel;
	public Image[] stars;

	public static bool paused;
	static ControllScript current;

	void Start() {
		current = this;
		paused = false;
	}

	public static void updateStars() {
		ScoreScript.setStars (current.stars, LevelScript.levelIndex, SlimeSpawn.spawnCouter);
	}

	public void loadNext() {
		Time.timeScale = 1;
		LevelScript.loadNext ();
	}

	public void pauseGame() {
		Time.timeScale = 0;
		paused = true;
		menuPanel.SetActive (true);
	}

	public void startLevel() {
		paused = false;
		menuPanel.SetActive (false);
		Time.timeScale = 1;
	}

	public void restartLevel() {
		Time.timeScale = 1;
		paused = false;
		SceneManager.LoadScene (SceneManager.GetActiveScene ().name);
	}

	public void mainMenu() {
		Time.timeScale = 1;
		SceneManager.LoadScene ("Menu");
	}

	public static void endScreen(float firstSpawn) {
		float levelTime = Time.timeSinceLevelLoad - firstSpawn;
		current.winPanelText.text = string.Format ("Time: {0:0.0}s", levelTime);
		LevelRecorder.Finish (levelTime);
		current.winPanel.SetActive (true);
		paused = true;
		/*
		int currentLevel = 0;//LevelSelectScript.levelNum;

		if (currentLevel+1 > PlayerPrefs.GetInt ("Progress", 0)) {
			PlayerPrefs.SetInt ("Progress", currentLevel + 1);
		}*/
	}


	public void Update() {
		if (Input.GetButtonDown ("Cancel")) {
			if (paused) {
				startLevel ();
			} else {
				pauseGame ();
			}
		} else if (Input.GetButtonDown ("Jump") && winPanel.activeSelf) {
			loadNext ();
		} else if(Input.GetKeyDown(KeyCode.R)) {
			restartLevel();
		}
	}
}
