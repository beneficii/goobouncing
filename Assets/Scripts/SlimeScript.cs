﻿using UnityEngine;
using System.Collections;

public class SlimeScript : MonoBehaviour {
	
	[HideInInspector]public int level = 0;
	[HideInInspector]public Rigidbody2D rb2d;
	[HideInInspector]public bool canStick = false;

	//private
	float xspeed = 3.5f;
	float xModifier = 1f;
	bool justJumped = false;
	float xvelocity = 0f;
	bool sticky = false;

	void Start() {
		rb2d = GetComponent<Rigidbody2D> ();
		GetComponent<Animator> ().SetInteger ("Level", level);
	}

	public void SpawnCompleeted() {
		xvelocity = xspeed;
		canStick = true;
		rb2d.bodyType = RigidbodyType2D.Dynamic;
	}

	void FixedUpdate () {
		transform.Translate((xvelocity*xModifier) * Time.fixedDeltaTime, 0f, 0f);
	}

	void Update() {
		if (transform.position.y < -0.5f) {
			Destroy (gameObject);
		}
	}

	public void flipSide(int dir) {
		xvelocity = dir * xspeed;
		GetComponent<SpriteRenderer> ().flipX = dir > 0;
	}

	public void setSpeedModifier(float x) {
		xModifier = x;
		justJumped = true;
		StartCoroutine (justJumpedExpire());
	}

	IEnumerator justJumpedExpire() {
		yield return new WaitForFixedUpdate();
		justJumped = false;
	}

	public void makeSticky() {
		SoundScript.play (Sound.BecomeSticky);
		sticky = true;
		GetComponent<SpriteRenderer>().color = new Color(1f,1f,1f,0.6f);
	}

	void OnCollisionEnter2D(Collision2D coll) {
		//print (coll.gameObject.tag + " " + justJumped.ToString ());
		if (coll.gameObject.tag == "Bouncer") {
			coll.gameObject.GetComponent<BouncerScript> ().Bounce (this);
		} else if (!justJumped && coll.gameObject.tag == "Platform") {
			xModifier = 1f;
		}
	}

	void OnTriggerEnter2D(Collider2D other) {
		if(other.tag == "Key") {
			GameScript.collectKey (other.gameObject);
		} else if(other.tag == "Exit") {
			GameScript.finishLevel ();
			if (GameScript.levelProgress > 0) {
				Destroy (gameObject);
			}
		} else if(other.tag == "Spikes") {
			SoundScript.play (Sound.Spikes);
			GetComponent<Animator> ().SetTrigger ("Death");
			xvelocity = 0;
			rb2d.bodyType = RigidbodyType2D.Static;
			canStick = false;
		} else if(other.tag == "PowerUp") {
			int newLevel = other.GetComponent<PowerUpScript> ().level;
			if (level != newLevel) {
				SoundScript.play (Sound.ChangeColor);
				level = newLevel;
				GetComponent<Animator> ().SetInteger ("Level", level);
				GetComponent<Animator> ().SetTrigger ("Morph");
			}
		}else if(other.tag == "Flip") {
			Flipper flipper = other.GetComponent<Flipper> ();
			if (!flipper.faded) {
				flipSide (xvelocity > 0 ? -1 : 1);
				flipper.Fade ();
			}
		}
	}

	public void destroy() {
		Destroy(gameObject);
	}

	void OnCollisionStay2D(Collision2D coll) {
		if(sticky && coll.gameObject.tag == "Platform") {
			SoundScript.play (Sound.Stick);
			sticky = false;
			Vector2 point = (Vector2)coll.collider.bounds.ClosestPoint (transform.position);
			Vector2 direction = point - (Vector2)transform.position;

			BouncerSpawn.spawn (level, point, direction, GetComponent<SpriteRenderer> ().flipX);
			Destroy (gameObject);
		}
	}
}
