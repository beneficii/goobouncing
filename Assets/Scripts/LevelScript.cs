﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

[System.Serializable]
public class LevelObject {
	[System.Serializable]
	public class MoveObject {
		public int lvl;
		public Vector2 pos;
		public int xd;
		public int yd;
		public bool flip;
	}
	public int id;
	public string map;
	public int minSpawns;
	public MoveObject[] moves;
	public float time;
}

public class LevelScript : MonoBehaviour {
	public TextAsset data;
	public TextAsset betaData;
	public InputField webInput;
	public GameObject buttonPrefab;

	static LevelObject[] levels;
	static LevelObject webLevel;
	public static int levelIndex;
	static bool onlineLevels = false;

	void Start() {
		//lOADING BETA LEVELS
		WrapperTmp wrapper = JsonUtility.FromJson<WrapperTmp>(betaData.text);
		levels = wrapper.levels;
		populateLevels ();
		/*
		if (onlineLevels) {
			StartCoroutine(DownloadAllLevels ());
		} else {
			string[] levelData = data.text.Split ('\n');
			levels = new LevelObject[levelData.Length];
			for (int i = 0; i < levelData.Length; i++) {
				levels[i] = JsonUtility.FromJson<LevelObject>(levelData[i]);
			}
			populateLevels();
		}*/
	}

	void populateLevels () {
		for (int i = 0; i < levels.Length; i++) {
			GameObject instance = Instantiate (buttonPrefab);
			instance.transform.SetParent (transform, false);
			instance.GetComponent<LevelButtonScript> ().level = i;
		}
	}

	[System.Serializable]
	public class WrapperTmp {
		public LevelObject[] levels;
	}

	IEnumerator DownloadAllLevels() {
		WWW www = new WWW("http://fancyreckless.org/cgi-bin/all/");

		yield return www;
		WrapperTmp wrapper = JsonUtility.FromJson<WrapperTmp>("{\"levels\": " + www.text + "}");
		levels = wrapper.levels;
		populateLevels ();
	}

	public void loadMyLevel() {
		levelIndex = -1;
		webLevel = new LevelObject ();
		webLevel.map = webInput.text;
		SceneManager.LoadScene ("main");
	}

	public static LevelObject getLevel() {
		if (levelIndex == -1) {
			return webLevel;
		}
		return levels [levelIndex];
	}

	public static LevelObject getLevel(int idx) {
		if (levelIndex == -1) {
			return webLevel;
		}
		return levels [idx];
	}

	public void loadLevel(int level) {
		levelIndex = level;
		SceneManager.LoadScene ("main");
	}

	public static void loadNext() {
		levelIndex++;
		if (levelIndex < levels.Length) {
			SceneManager.LoadScene ("main");
		} else {
			SceneManager.LoadScene ("Levels");
		}
	}

	public void ToggleOnline() {
		onlineLevels = !onlineLevels;
		SceneManager.LoadScene ("Levels");
	}

	
}
