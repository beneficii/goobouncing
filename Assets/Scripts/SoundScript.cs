﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Sound {
	JumpBasic = 0,
	JumpHigh,
	JumpLong,
	JumpTeleport,
	ChangeColor,
	BecomeSticky,
	Stick,
	Spikes,
	GetKey,
	FinishLevel,
	Spawn
}
public class SoundScript : MonoBehaviour {
	public AudioClip[] clips;


	static SoundScript instance;

	void Awake() {
		if (instance != null && instance != this) {
			Destroy(this.gameObject);
			return;
		} else {
			instance = this;
		}
		DontDestroyOnLoad(this.gameObject);
	}

	public static void play (Sound sound) {
		instance.playSound ((int)sound);
	}

	void playSound(int index) {
		GetComponent<AudioSource> ().PlayOneShot (clips[index]);
	}
}
