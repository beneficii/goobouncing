﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class LevelButtonScript : MonoBehaviour {

	[HideInInspector]public int level;
	public Text levelCaption;
	public Image[] stars;

	void Start() {
		if (level > ScoreScript.scrLevelsBeaten.getScore()) {
			gameObject.SetActive (false);
		} else {
			levelCaption.text = level.ToString ();
			ScoreScript.setStars (stars, level);
		}
	}
	
	public void OnClick () {
		transform.parent.GetComponent<LevelScript> ().loadLevel (level);
	}
}
