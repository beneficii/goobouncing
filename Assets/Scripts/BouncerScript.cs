﻿using UnityEngine;
using System.Collections;


public enum BouncerPos {Top, Down, Left, Right};

public class BouncerScript : MonoBehaviour {

	//[HideInInspector]public Vector2 portalPoint;
	[HideInInspector]public int xDirection;
	[HideInInspector]public int yDirection;
	[HideInInspector]public int level = -1;
	[HideInInspector]public Sound triggerSound;
	Animator anim;

	void Start() {
		anim = GetComponent<Animator> ();
		anim.SetInteger ("Level", level);
	}

	public void Bounce(SlimeScript slime) {
		anim.SetTrigger ("Bounce");
		SoundScript.play (triggerSound);
		GetComponent<IBouncer> ().Bounce (slime, xDirection, yDirection);
	}
}
