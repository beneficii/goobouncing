﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BouncerSpawn : MonoBehaviour {

	public GameObject BouncerPrefab;
	static GameObject prefab;

	void Start() {
		prefab = BouncerPrefab;
	}

	static GameObject bouncerInstance(int lvl, Vector2 pos, int xDirection, int yDirection, bool flip) {
		GameObject instance = Instantiate (prefab);
		instance.transform.position = pos;
		instance.transform.up = new Vector3 (xDirection, yDirection);
		BouncerScript script = instance.GetComponent<BouncerScript> ();
		script.xDirection = xDirection;
		script.yDirection = yDirection;
		script.level = lvl;

		switch (lvl) {
		case 0:
			script.triggerSound = Sound.JumpBasic;
			break;
		case 1:
			script.triggerSound = Sound.JumpTeleport;
			break;
		case 2:
			script.triggerSound = Sound.JumpHigh;
			break;
		case 3:
			script.triggerSound = Sound.JumpLong;
			break;
		}

		instance.GetComponent<SpriteRenderer> ().flipX = flip;
		return instance;
	}

	public static void spawn(int lvl, Vector2 pos, Vector2 v, bool flip, bool userPlaced = true) {

		SoundScript.play (Sound.Stick);

		//find direction
		int xDirection, yDirection;
		if (Mathf.Abs (v.x) > Mathf.Abs (v.y)) { //horisontal
			xDirection = 1;
			yDirection = 0;
			if (v.x > 0) {
				xDirection = -1;
			}
		} else {								//vertical
			xDirection = 0;
			yDirection = 1;
			if (v.y > 0) {
				yDirection = -1;
			}
		}
		if (userPlaced) {
			LevelRecorder.AddMove (lvl, pos, xDirection, yDirection, flip);
		}
		SpawnInstance (lvl, pos, xDirection, yDirection, flip);
	}

	public static void SpawnInstance(int lvl, Vector2 pos, int xDirection, int yDirection, bool flip) {
		GameObject instance = bouncerInstance (lvl, pos, xDirection, yDirection, flip);

		switch (lvl) {
		case 0:
			instance.AddComponent<BasicBouncer> ();
			break;
		case 1:
			makeTeleport (instance);
			break;
		case 2:
			instance.AddComponent<HighBouncer> ();
			break;
		case 3:
			instance.AddComponent<LongBouncer> ();
			break;
		}

	}

	public static void AddMapBouncer(int level, Vector2 pos) {
		//down, right, left, up
		float distance = 1f;
		RaycastHit2D hit = Physics2D.Linecast (pos, pos + Vector2.down*distance);

		if (!hit.collider) {
			hit = Physics2D.Linecast (pos, pos + Vector2.right*distance);
			if (!hit.collider) {
				if (!hit.collider) {
					hit = Physics2D.Linecast (pos, pos + Vector2.left*distance);
					if (!hit.collider) {
						hit = Physics2D.Linecast (pos, pos + Vector2.up*distance);
						if (!hit.collider) {
							Debug.LogError ("Could not find wall for map bouncer!");
							return;
						}
					}
				}
			}
		}

		spawn (level, hit.point, hit.point-pos, false, false);
	}

	static void makeTeleport(GameObject bouncer) {
		Vector2 pos = bouncer.transform.position;
		Vector2 down = -bouncer.transform.up;
		int xDirection = bouncer.GetComponent<BouncerScript> ().xDirection;
		int yDirection = bouncer.GetComponent<BouncerScript> ().yDirection;
		bool flip = bouncer.GetComponent<SpriteRenderer> ().flipX;
		RaycastHit2D hit = Physics2D.Linecast (pos + down * 0.5f, pos + down * 20f);

		if (hit.collider) {
			GameObject mirror = bouncerInstance (1, hit.point, -xDirection, -yDirection, flip);
			bouncer.AddComponent<TeleportBouncer> ().endpoint = mirror.transform.position + mirror.transform.up*0.5f;
			mirror.AddComponent<TeleportBouncer> ().endpoint = bouncer.transform.position + bouncer.transform.up*0.5f;
		} else {
			Debug.LogError ("Teleport didn't connect!");
		}
	}
}
