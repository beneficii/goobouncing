﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class GameScript : MonoBehaviour {
	public static int levelProgress = 0;
	public int helpIndex = 0;

	Queue<SlimeScript> currentSlime;

	static float firstSpawn;

	void Start() {
		currentSlime = new Queue<SlimeScript> ();
		levelProgress = 0;
		firstSpawn = -1f;
		//generate map
	}

	public void Update() {
		if (Input.GetButtonDown ("Jump") && !ControllScript.paused) {
			onClick ();
		} /*else if(Input.GetKeyDown(KeyCode.A)) {
			spawnSlime (0);
		} else if(Input.GetKeyDown(KeyCode.S)) {
			spawnSlime (0);
		} else if(Input.GetKeyDown(KeyCode.D)) {
			spawnSlime (2);
		} else if(Input.GetKeyDown(KeyCode.F)) {
			spawnSlime (2);
		}*/
	}

	public void spawnSlime(int lvl) {
		if (!ControllScript.paused) {
			while (currentSlime.Count > 0 && currentSlime.Peek () == null) {
				currentSlime.Dequeue ();
			}
			if (currentSlime.Count > 0) {
				onClick ();
			} else {
				currentSlime.Enqueue (SlimeSpawn.spawn (lvl));
			}
		}
	}

	public void onClick () {
		if (Time.timeScale != 0 && Time.timeSinceLevelLoad > 0.3f) {
			while (currentSlime.Count > 0 && currentSlime.Peek () == null) {
				currentSlime.Dequeue ();
			}
			if (currentSlime.Count > 0 && currentSlime.Peek ().canStick) {
				currentSlime.Peek ().makeSticky ();
				currentSlime.Dequeue ();
			}
			if (currentSlime.Count == 0) {
				if (firstSpawn < 0f) {
					firstSpawn = Time.timeSinceLevelLoad;
				}
				currentSlime.Enqueue (SlimeSpawn.spawn (0));
			}
		}
	}

	public static void collectKey(GameObject key) {
		if (levelProgress == 0) {
			SoundScript.play (Sound.GetKey);
			Destroy (key);
			GameObject.FindGameObjectWithTag ("Exit").GetComponent<Animator> ().SetTrigger ("Open");
			levelProgress = 1;
		}
	}

	public static void finishLevel() {
		if (levelProgress == 1) {
			SoundScript.play (Sound.FinishLevel);
			levelProgress = 2;
			ScoreScript.levelFinished ();
			ControllScript.endScreen (firstSpawn);
		}
	}

	public void GetHelp() {
		LevelObject level = LevelScript.getLevel ();
		if (helpIndex < level.moves.Length) {
			LevelObject.MoveObject move = LevelScript.getLevel ().moves [helpIndex];
			BouncerSpawn.SpawnInstance (move.lvl, move.pos, move.xd, move.yd, move.flip);
			helpIndex++;
		}
	}
}
