﻿using UnityEngine;
using System.Collections;

public class PowerUpScript : MonoBehaviour {

	public Sprite[] sprites;
	[HideInInspector]
	public int level;

	public void init(int type) {
		level = type;
		GetComponentInChildren<SpriteRenderer> ().sprite = sprites [level];
	}
}
