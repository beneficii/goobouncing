﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlimeSpawn : MonoBehaviour {
	public GameObject SlimePrefab;
	public Sprite[] sprites;
	public static int spawnCouter;
	public static SlimeSpawn current;

	Vector3 spawnPoint;
	Animator anim;

	int _level = 0;
	public int level {
		get{ return _level; }
		set{
			_level = value;
			GetComponent<SpriteRenderer> ().sprite = sprites [_level];
		}
	}


	void Start() {
		current = this;
		spawnPoint = transform.position;
		spawnCouter = 0;
	}

	SlimeScript SpawnSlime() {
		SoundScript.play (Sound.Spawn);
		spawnCouter++;
		ControllScript.updateStars ();
		GameObject instance = Instantiate (SlimePrefab);
		instance.transform.position = spawnPoint;
		SlimeScript script = instance.GetComponent<SlimeScript> ();
		script.level = level;
		return script;
	}

	public static SlimeScript spawn(int foo) {
		return current.SpawnSlime ();
	}
}
